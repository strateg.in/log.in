// Express
const express = require("express");
const app = express();

//EJS
app.set("view engine", "ejs");

//Bodyparser
const bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

//Cookieparser
const cookieParser = require("cookie-parser");
app.use(cookieParser());

//bcrypt et JWT
const bcrypt = require("bcrypt");
const saltRounds = 3;
const jwt = require("jsonwebtoken");
const SECRET = "my key";

// Vérification du token
const verifyToken = (request, response, next) => {
  const token = request.cookies.token_jwt;
  jwt.verify(token, SECRET, (err, decoded) => {
    if (!token) {
      return response
        .status(403)
        .json({ message: "Token manquant dans le cookie" });
    }
    if (err) {
      return response.status(401).json({ message: "Token invalide" });
    }
    request.user = decoded;
    next();
  });
};

//static
app.use("/static", express.static("./static"));

//mongoose
const mongoose = require("mongoose");
const { request } = require("express");
main().catch((err) => console.log(err));
async function main() {
  await mongoose.connect(
    "mongodb+srv://lverdierweb:h5tGhee6AElybT3x@cluster0.oq0otgx.mongodb.net/?retryWrites=true&w=majority"
  );
}
const { Schema } = mongoose;

const userSchema = new Schema({
  identifiant: String,
  email: String,
  password: String,
});
const User = mongoose.model("User", userSchema);

app.get("/", (request, response) => {
  response.redirect("/login");
});
app.get("/login", (request, response) => {
  response.render("login", { tagline: "" });
});

app.post("/login", async (request, response) => {
  const searchUser = await User.findOne({ email: request.body.email });
  if (
    searchUser != null &&
    bcrypt.compareSync(request.body.password, searchUser.password)
  ) {
    const token = jwt.sign(
      { userId: searchUser.identifiant, userEmail: searchUser.email },
      SECRET
    );
    response.cookie("token_jwt", token, { httpOnly: true }).redirect("/users");
  } else {
    response.render("login", {
      tagline: "email et/ou mot de passe invalide(s) !",
    });
  }
});

app.get("/register", (request, response) => {
  response.render("register", { tagline: "" });
});

app.post("/register", async (request, response) => {
  const user = new User(request.body);
  const searchUser = await User.find({ email: user.email });
  if (searchUser == false) {
    if (request.body.password == request.body.passwordconfirm) {
      const hashPassword = bcrypt.hashSync(user.password, saltRounds);
      user.password = hashPassword;
      await user.save();
      response.render("login", {
        tagline: "Le compte utilisateur a bien été créé ! ",
      });
    } else {
      response.render("register", {
        tagline: "le mot de passe et la confirmation ne sont pas identiques !",
      });
    }
  } else {
    response.render("register", {
      tagline: "Cet e-mail est déjà utilisé par un autre utilisateur !",
    });
  }
});

app.get("/users", verifyToken, async (request, response) => {
  const usersList = await User.find();
  response.render("users", { usersList: usersList });
});
app.post("/users", (request, response) => {
  response.clearCookie("token_jwt");
  response.render("login", {
    tagline: "Vous avez bien été déconnecté de la platforme !",
  });
});

app.listen(3002, () => {
  console.log("Server listening on localhost:3002");
});
